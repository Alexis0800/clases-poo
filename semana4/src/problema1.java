import java.util.Scanner;

public class problema1 {
    public static void muestranotasbajas(int[] notas, int cantidadmostrar){
        for(int i=0; i<notas.length-1; i++){
            for(int j=i+1; j<notas.length; j++){
                if(notas[i]>notas[j]){
                    int aux=notas[i];
                    notas[i]=notas[j];
                    notas[j]=aux;
                }
            }
        }
        for(int i=0; i<cantidadmostrar; i++){
            System.out.println(notas[i]);
        }
    }
    public static void main(String[] args) {
        int[] notas = {12, 14, 11, 8, 19, 13, 17, 10, 5};
        System.out.println("Ingrese n: ");
        Scanner n = new Scanner(System.in);
        int s = n.nextInt();
        muestranotasbajas(notas, s);
    }
}